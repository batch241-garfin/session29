
// Query Operators

// $gt / $gte operator

// Allows us to have documents that have field number values greater than or equal to a specified value.

// Syntax:
	// db.collectionName.find({field: {$gt: value}});
	// db.collectionName.find({field: {$gte: value}});

db.users.find({age: {$gt:65}});
db.users.find({age: {$gte:65}});

// $lt / $lte operator

// Allows us to find documents that have field number less than or equal to a specified value.

// Syntax:
	// db.collectionName.find({field: {$lt: value}});
	// db.collectionName.find({field: {$lte: value}});

db.users.find({age: {$lt:76}});
db.users.find({age: {$lte:76}});

// $ne operator

// Allows us to find documents that have a specified value that are not equal to a specified value.

// Syntax:
	// db.collectionName.find({field:$ne:value});

db.users.find({age:{$ne:82}});

// Logical Query operators

// $or

// Allows us to find documents that match a single criteria from multiple search criteria provided.

// Syntax:
	// db.collectionName.find({$or: [{fieldA; "ValueA"},{fieldB: "valueB"}]});

db.users.find({$or: [{firstName: "Neil"},{firstName: "Bill"}]});
db.users.find({$or: [{firstName: "Neil"},{firstName: "Bill"},{firstName: "Stephen"}]});

db.users.find({$or: [{firstName: "Neil"},{age:{$gt:30}}]});

// $and operator

// Allows us to find documents matching criteria in a singlle field

// Synatx: 
	// db.collectionName.find({$and:[{fieldA: valueA},{fieldB: valueB}]});

db.users.find
(
	{
		$and:
		[
			{
				age:
				{
					$ne:82
				}
			},
			{
				age:
				{
					$ne:76
				}
			}
		]
	}
);


db.users.find(
	{$and:
	[
		{age:{$ne:82}},
		{age:{$ne:76}}
	]
});


// Field Projection

// When we retrieve documents, MongoDB usually returns the whole documents.
// there are times when we only need spcific fields.
// For those cases, we can include or exclude fields from the response.

// Inclusion

// Allows us to include / add specific fields only when retrieving documents.
// We write 1 to include fields

// Syntax:
	// db.users({criteria},{field:1});

db.users.find(
	{
		firstName: "Jane"
	},
	{
		firstName: 1,
		lastName: 1,
		"contact.phone": 1
	}
);

// Exclusion

// Allows us to exclude or remove specific fields when displaying documents
// The value provided is 0 to denote that the field is being excluded.

// Syntax:
	// db.users.find({criteria},{field:0});


db.users.find(
	{
		firstName: "Jane"
	},
	{
		"contact.phone": 0,
		department: 0
	}
);

// Suppressing the ID Field

// Allows us to exclude the id field when retrieving documents.
// When using field projection, field inclusion and exclusion may not be used at the same time.
// The _id is exempted to this rule.

// Syntax:
	// db.users.find({criteria},{_id:0});

db.user.find(
{
	firstName: "Jane"
},
{
	firstName: 1,
	lastName: 1,
	contact: 1,
	_id: 0
}
);

// Evaluation of Query Operatrs

// $regex operator

// Allows us to find documents that match a specific string pattern using regular expressions.

// Syntax:
	// db.users.find({field: $regex: 'pattern', $options: '$optionsValue'});

// Case Sensitive Query
db.users.find({firstName: {$regex: 'N'}});

// Case INsensitive Query
db.users.find({firstName: {$regex: 'N', $options:'$i'}});



